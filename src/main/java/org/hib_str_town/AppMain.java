package org.hib_str_town;

import org.hib_str_town.config.AppConfig;
import org.hib_str_town.model.Street;
import org.hib_str_town.model.Town;
import org.hib_str_town.service.StreetService;
import org.hib_str_town.service.TownService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class AppMain {

    public static void main(String[] args) {
        AbstractApplicationContext abstractApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        StreetService streetService = (StreetService) abstractApplicationContext.getBean("streetService");
        TownService townService = (TownService) abstractApplicationContext.getBean("townService");
//        Street street1 = new Street("Verhovinna");
//        Street street2 = new Street("Mazepi");
//
//        Town town1 = new Town("Kiev");
//        Town town2 = new Town("Dnipro");
//        Town town3 = new Town("Lviv");
//
//        List<Town> towns1 = new ArrayList<>();
//        towns1.add(town1);
//        towns1.add(town2);
//
//        street1.setTowns(towns1);
//
//        List<Town>towns2 = new ArrayList<>();
//        towns2.add(town1);
//        towns2.add(town2);
//        towns2.add(town3);
//
//        street2.setTowns(towns2);
//
//        streetService.save(street1, street2);

//        Town town = new Town("Fastiv");
//        townService.save(town);

//        System.out.println(streetService.findById(2));
//
//        List<Street>streets = streetService.findAllStreets();
//        for (Street s: streets) {
//            System.out.println(s);
//        }

//        List<Town>towns = townService.findAll();
//        for (Town town:towns) {
//            System.out.println(town);
//        }
//
//        Town town4 = new Town("Bila Tserkva");
//        townService.save(town4);

        List<Town> towns1 = townService.findAll();
        for (Town town:towns1) {
            System.out.println(town);
        }

        System.out.println(townService.findByName("Fastiv"));
        System.out.println(streetService.findById(2));
        abstractApplicationContext.close();
    }
}
