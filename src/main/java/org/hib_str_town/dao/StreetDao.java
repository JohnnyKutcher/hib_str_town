package org.hib_str_town.dao;

import org.hib_str_town.model.Street;

import java.util.List;

public interface StreetDao {

    Street findById(long id);

    void save(Street... street);

    void deleteByName(String name);

    List<Street> findAllStreets();

}
