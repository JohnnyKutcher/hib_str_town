package org.hib_str_town.dao;

import org.hib_str_town.model.Street;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("streetDao")
public class StreetDaoImpl extends AbstractDao implements StreetDao {

    @Override
    public Street findById(long id) {
        Criteria criteria = getSession().createCriteria(Street.class);
        criteria.add(Restrictions.eq("id", id));
        return (Street) criteria.uniqueResult();
    }

    @Override
    public void save(Street... street) {
        for (Street s:street) {
            persist(s);
        }
    }

    @Override
    public void deleteByName(String name) {
        Query query = getSession().createSQLQuery("delete from STREET where name = :name");
        query.setString("name", name);
        query.executeUpdate();
    }

    @Override
    public List<Street> findAllStreets() {
        Criteria criteria = getSession().createCriteria(Street.class);
        return (List<Street>) criteria.list();
    }
}
