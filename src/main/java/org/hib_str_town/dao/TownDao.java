package org.hib_str_town.dao;

import org.hib_str_town.model.Town;

import java.util.List;

public interface TownDao {

    List<Town> findAll();

    Town findByName(String name);

    void save(Town... towns);


}
