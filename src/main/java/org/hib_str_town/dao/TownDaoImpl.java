package org.hib_str_town.dao;

import org.hib_str_town.model.Town;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("townDao")
public class TownDaoImpl extends AbstractDao implements TownDao{

    @Override
    public List<Town> findAll() {
        Criteria criteria = getSession().createCriteria(Town.class);
        return (List<Town>) criteria.list();
    }

    @Override
    public Town findByName(String name) {
        Criteria criteria = getSession().createCriteria(Town.class);
        criteria.add(Restrictions.eq("name", name));
        return (Town) criteria.uniqueResult();
    }

    @Override
    public void save(Town... towns) {
        for (Town t:towns) {
            persist(t);
        }
    }

}
