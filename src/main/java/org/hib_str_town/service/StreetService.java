package org.hib_str_town.service;

import org.hib_str_town.model.Street;

import java.util.List;

public interface StreetService {

    Street findById(long id);

    void save(Street... street);

    void deleteByName(String name);

    List<Street> findAllStreets();
}
