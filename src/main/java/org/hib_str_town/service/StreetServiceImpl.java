package org.hib_str_town.service;

import org.hib_str_town.dao.StreetDao;
import org.hib_str_town.model.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("streetService")
@Transactional
public class StreetServiceImpl implements StreetService {
    
    @Autowired
    private StreetDao streetDao;
    
    @Override
    public Street findById(long id) {
        return streetDao.findById(id);
    }

    @Override
    public void save(Street... street) {
        streetDao.save(street);
    }

    @Override
    public void deleteByName(String name) {
        streetDao.deleteByName(name);
    }

    @Override
    public List<Street> findAllStreets() {
        return streetDao.findAllStreets();
    }
}
