package org.hib_str_town.service;

import org.hib_str_town.model.Town;

import java.util.List;

public interface TownService {
    
    List<Town> findAll();

    Town findByName(String name);

    void save(Town... towns);


}
