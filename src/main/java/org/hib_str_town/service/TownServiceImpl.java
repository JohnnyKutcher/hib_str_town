package org.hib_str_town.service;

import org.hib_str_town.dao.TownDao;
import org.hib_str_town.model.Town;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("townService")
@Transactional
public class TownServiceImpl implements TownService {
    
    @Autowired
    private TownDao townDao;
    
    @Override
    public List<Town> findAll() {
        return townDao.findAll();
    }

    @Override
    public Town findByName(String name) {
        return townDao.findByName(name);
    }

    @Override
    public void save(Town... towns) {
        townDao.save(towns);
    }
}
